package org.g4.shared_preferences_canicat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Main";
    EditText textColors;
    EditText textGenders;
    EditText textName;
    EditText textAge;
    EditText textRace;
    Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textColors = findViewById(R.id.spinnerColors);
        textGenders = findViewById(R.id.spinnerGenders);
        textName = findViewById(R.id.textName);
        textAge = findViewById(R.id.textAge);
        textRace = findViewById(R.id.textRace);
        buttonSave = findViewById(R.id.buttonSave);

        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        textName.setText(sharedPref.getString(getString(R.string.saved_pet_name), ""));
        textAge.setText(sharedPref.getString(getString(R.string.saved_pet_age), ""));
        textRace.setText(sharedPref.getString(getString(R.string.saved_pet_race), ""));
        textGenders.setText(sharedPref.getString(getString(R.string.saved_pet_gender), ""));
        textColors.setText(sharedPref.getString(getString(R.string.saved_pet_color), ""));

        buttonSave.setOnClickListener(this::saveOnSharedPreferences);
    }

    public void saveOnSharedPreferences(View view) {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_pet_name), textName.getText().toString());
        editor.putString(getString(R.string.saved_pet_age), textAge.getText().toString());
        editor.putString(getString(R.string.saved_pet_race), textRace.getText().toString());
        editor.putString(getString(R.string.saved_pet_gender), textGenders.getText().toString());
        editor.putString(getString(R.string.saved_pet_color), textColors.getText().toString());
        editor.commit();
        finish();
    }

//    public void setSpinnerAdapters() {
//        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> colorsAdapter = ArrayAdapter.createFromResource(this,
//                R.array.colors_array, android.R.layout.simple_spinner_item);
//        ArrayAdapter<CharSequence> gendersAdapter = ArrayAdapter.createFromResource(this,
//                R.array.genders_array, android.R.layout.simple_spinner_item);
//
//        // Specify the layout to use when the list of choices appears
//        colorsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        gendersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        // Apply the adapter to the spinner
//        textColors.setAdapter(colorsAdapter);
//        textGenders.setAdapter(gendersAdapter);
//    }
}